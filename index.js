console.log("Hello World");

let number = Number(prompt("Enter a number"));
console.log("The number you provided is " + number);

for (let i = number; i >= 0; i--) {

	if (i % 10 === 0) {

		if (i <= 50) {
		console.log("The current value is at " + i + ". Terminating the loop.");
		break;
		}

		console.log("The " + i + " is divisible by 10. Skipping the number.");

		continue;
	}
	
	if (i % 5 === 0) {
		console.log(i);	
	}

	if (i <= 50) {
		console.log("The current value is at " + i + ". Terminating the loop.");
		break;
		}


}


let myString = "supercalifragilisticexpialidocious"

let myStringConsonants = "";

for (i = 0; i < myString.length; i++) {
	if (myString[i] === "a" || myString[i] === "e" || myString[i] === "i" || myString[i] === "o" || myString[i] === "u") {

		continue;
	} else {
		myStringConsonants += myString[i];   
	}
}

console.log(myString);
console.log(myStringConsonants);